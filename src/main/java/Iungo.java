import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.reactivex.Observable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.WatchEvent;

public class Iungo {

    private static String API_TELEGRAM = "https://api.telegram.org/bot[TOKEN]";

    public static String TO_IUNGO = "/home/cristian/tmp/prx/in/";
    private static String DONE_IUNGO = "/home/cristian/tmp/prx/done/";

    public static Observable<Order> getOrders() {
        return Observable.create(subscriber -> {
            try {
                File toIungo = new File(TO_IUNGO);
                JAXBContext jc = JAXBContext.newInstance(Order.class);
                Unmarshaller unmarshaller = jc.createUnmarshaller();

                for (File file : toIungo.listFiles()) {
                    Order order = (Order) unmarshaller.unmarshal(file);
                    subscriber.onNext(order);

                }
                subscriber.onComplete();

            } catch(Exception e) {
                subscriber.onError(e);
            }
        });
    }


    public static void sendOrder(Order order) {
        String template = String.format("*Nuovo ordine da parte di iungogram*\n item: %s\n qty: %s \n price: %s\n",
                order.getName(),
                order.getQty(),
                order.getPrice());
        try {
            Unirest.post(API_TELEGRAM + "/sendMessage")
                    .queryString("chat_id", order.getSupplier())
                    .queryString("text", template)
                    .queryString("parse_mode","markdown")
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    public static Order getOrder(WatchEvent ev) {
        Order order = null;
        try {
            Path p = (Path) ev.context();
            File f = new File(TO_IUNGO + p.getFileName().toString());

            JAXBContext jc = JAXBContext.newInstance(Order.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            order = (Order) unmarshaller.unmarshal(f);
            order.setFile(f.getAbsolutePath());
            return order;


        } catch (Exception e) {
            System.out.println(e);
        }

        return order;
    }

    public static Order moveOrder(Order o) {
        File order = new File(o.getFile());
        File done = new File(DONE_IUNGO +order.getName());
        order.renameTo(done);
        order.delete();
        return o;
    }
}
