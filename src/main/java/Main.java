import io.reactivex.Observable;

import java.io.IOException;
import java.nio.file.*;

public class Main {



    public static void main(String [] Args) throws InterruptedException, IOException {

        FileSystem fileSystem = FileSystems.getDefault();
        WatchService watcher = fileSystem.newWatchService();

        Path myDir = fileSystem.getPath(Iungo.TO_IUNGO);
        myDir.register(watcher,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY);

        WatchKey watckKey = watcher.take();

        while (true) {
            Observable<Order> orders = Observable.fromIterable(watckKey.pollEvents())
                    .filter(ev -> ev.kind() == StandardWatchEventKinds.ENTRY_CREATE)
                    .map(Iungo::getOrder)
                    .doOnNext(Iungo::moveOrder);


            orders.doOnNext(Main::getName)
                    .subscribe(Iungo::sendOrder);
        }

    }

    private static void getQty(Order order) {
        System.out.println("Quantity is " + order.getQty());
    }

    private static void getName(Order order) {
        System.out.println("Item is " + order.getName());
    }

}

